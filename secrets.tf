#----------------------------------------------------------
# Enable secrets engines
#----------------------------------------------------------

# Enable K/V v2 secrets engine at 'kv-v2'
resource "vault_mount" "kv-v2" {
  path = "kv-v2"
  type = "kv-v2"
}

resource "vault_generic_secret" "minio_user" {
  path = "kv-v2/minio"

  data_json = <<EOT
{
  "username":   "${var.minio_user}",
  "password": "${random_password.minio_pass.result}"
}
EOT
}