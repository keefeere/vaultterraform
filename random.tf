resource "random_password" "admin_pass" {
  length  = 8
  special = false
  upper   = true
}


resource "random_password" "minio_pass" {
  length  = 8
  special = false
  upper   = true
}
