terraform {
  backend "consul" {
    address = "http://debianvm:8500"
    scheme  = "http"
    lock = true
    path    = "terraform/terraformvault.tfstate"
  }
}
