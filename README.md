### **Homework 5. Part 1. Vault provisioner**

## Vault terraform provision

Features:

*  Remote state storage
*  Enables Vault userpass autentification
*  Creates user for Vault autentification, assign policy
*  Creates random data for user passwords
*  Creates Kv secrets with username and password for [MiniO](https://min.io/) provision
*  Outputs created user data
