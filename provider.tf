
provider "vault" {
  version = "~> 2.8"
  address = var.vault_address
  #Assumig that inital root token is passed through the environment variable VAULT_TOKEN
}

provider "random" {
#source = "hashicorp/random"
      version = "2.3.0"
}