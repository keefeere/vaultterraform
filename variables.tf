
variable "vault_address" {
    type = string
    description = "Vault instance address"
}


variable "minio_user" {
    type = string
    description = "User for minio"
}


/* 
variable "admin_pass" {
    type = string
    description = "Password of admin account"
    value = random_string.admin_pass.result
}



variable "minio_password" {
    type = string
    description = "password for minio"
    value = random.minio_password.result
} */